#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SERVER_HOST "127.0.0.1" // default server IP: localhost
#define SERVER_PORT 1234         // fixed server port number
#define BUFLEN 256               // max length of buffer

int main() {
    char line[BUFLEN];
    struct sockaddr_in server;
    int sock, rlen, slen = sizeof(server);

    printf("1. create a UDP socket\n");
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    printf("2. fill in server address and port number\n");
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(SERVER_PORT);
    if (inet_aton(SERVER_HOST, &server.sin_addr) == 0) {
        fprintf(stderr, "Invalid address\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("Enter a line: ");
        fgets(line, BUFLEN, stdin);
        line[strlen(line) - 1] = '\0';

        printf("send line to server\n");
        if (sendto(sock, line, strlen(line), 0, (struct sockaddr *)&server, slen) < 0) {
            perror("sendto failed");
            exit(EXIT_FAILURE);
        }

        memset(line, 0, BUFLEN);
        printf("try to receive a line from server\n");
        if ((rlen = recvfrom(sock, line, BUFLEN, 0, (struct sockaddr *)&server, (socklen_t *)&slen)) < 0) {
            perror("recvfrom failed");
            exit(EXIT_FAILURE);
        }
        printf("rlen=%d: line=%s\n", rlen, line);
    }

    return 0;
}

