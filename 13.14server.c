#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define BUFLEN 25
#define PORT 1234

int main() {
    char line[BUFLEN];
    struct sockaddr_in me, client;
    int sock, rlen, clen = sizeof(client);

    printf("1. create a UDP socket\n");
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    printf("2. fill 'me' with server address and port number\n");
    memset(&me, 0, sizeof(me));
    me.sin_family = AF_INET;
    me.sin_port = htons(PORT);
    me.sin_addr.s_addr = htonl(INADDR_ANY); // use localhost

    printf("3. bind socket to server IP and port\n");
    if (bind(sock, (struct sockaddr *)&me, sizeof(me)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    
    printf("4. wait for datagram\n");
    while (1) {
        memset(line, 0, BUFLEN);
        printf("UDP server: waiting for datagram\n");

        // recvfrom() gets client IP, port in sockaddr_in client
        rlen = recvfrom(sock, line, BUFLEN, 0, (struct sockaddr *)&client, (socklen_t *)&clen);
        printf("received a datagram from [host:port] = [%s:%d]\n",
               inet_ntoa(client.sin_addr), ntohs(client.sin_port));
        printf("rlen=%d: line=%s\n", rlen, line);
        printf("send reply\n");
        sendto(sock, line, rlen, 0, (struct sockaddr *)&client, clen);
    }
    return 0;
}

